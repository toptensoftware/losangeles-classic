var merge = require('utils-merge');
var debug = require('debug')('losangeles:pageDefaults');
var pathToRegexp = require('path-to-regexp');

/*
 * Processes an array of "default" settings
 *
 * Each entry in the array should be an object with the following properties:
 * 
 * - urlPrefix - an Express style path expression to match at the start of the url
 * - urlMatch - an Express style path expression to match on the entire url
 * - data - an object to be merged into the page defaults
 * 
 * Final set of defaults stored on ctx.pageDefaults.  Any params in the urlPrefix
 * or urlMatch expression will be included in the pageDefaults.
 * 
 * Will be merged with ctx.page later in mergePageDefaults filter.
 */

module.exports = function initPageDefaults(defaults)
{
	debug(defaults);

	return function pageDefaultsFilter(ctx, next)
	{
		if (!defaults)
			return next();

		var pageDefaults = ctx.pageDefaults = {
			view: "page",
			contentType: "text/markdown",
		};

			debug(defaults);


		for (var i=0; i<defaults.length; i++)
		{
			var d = defaults[i];

			debug("matching", ctx.url, "vs", i, d);

			// Work out if matches
			if (d.urlPrefix || d.urlMatch)
			{
				// Build a regex to match the path
				var rx = d._urlPrefixRegex;
				if (!rx)
				{
					var keys = [];
					d._urlPrefixRegex = rx = pathToRegexp(
						d.urlPrefix ? d.urlPrefix : d.urlMatch, 
						keys,
						{ end: !d.urlPrefix }
						)
				}

				// Test it
				var m = rx.exec(ctx.url);
				if (!m)
				{
					debug("didn't match");
					continue;
				}

				// Build params
				for (var j=1; j<m.length; j++)
				{
					var prop = rx.keys[j-1].name;
					var value = m[j];
					pageDefaults[prop] = value;
				}
			}

			// Default data
			if (d.data)
			{
				merge(pageDefaults, d.data);
			}

			// Invoke callback?
			if (d.callback)
			{
				d.callback(ctx);
			}
		}

		debug("page defaults:", pageDefaults);
		next();
	}
}