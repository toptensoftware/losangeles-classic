var merge = require('utils-merge');


/* 
 * Merges ctx.pageDefaults with ctx.page
 */
 
module.exports = function(ctx, next)
{
	if (!ctx.pageDefaults)
		return next();

	if (ctx.page)
	{
		ctx.page = merge(ctx.pageDefaults, ctx.page)
	}

	next();
}