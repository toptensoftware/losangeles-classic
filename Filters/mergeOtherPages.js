var path = require('path');
var merge = require('utils-merge');
var debug = require('debug')('losangeles:mergeOtherPages');

module.exports = function(ctx, next)
{
	if (!ctx.page || !ctx.page.merge)
		return next();

	var mergeInfo = ctx.page.merge;
	debug("page:", ctx.page);
	debug("merge:", mergeInfo);

	var folder = path.dirname(ctx.page.filename);
	var pending = 1;

	function finish()
	{
		pending--;
		if (pending==0)
		{
			next();
		}
	}

	if (mergeInfo.before)
	{
		var ext = path.extname(mergeInfo.before);
		var parser = ctx.la.fileParsers[ext];
		if (parser)
		{
			pending++;
			parser(path.join(folder, mergeInfo.before), function(err, page) {

				if (err)
					return next(err);

				ctx.page.body = page.body + "\n\n" + ctx.page.body;
				ctx.page = merge(page, ctx.page);
				finish();
			});
		}
	}

	if (mergeInfo.after)
	{
		var ext = path.extname(mergeInfo.after);
		var parser = ctx.la.fileParsers[ext];
		if (parser)
		{
			pending++;
			parser(path.join(folder, merge.after), function(err, page) {

				if (err)
					return next(err);

				ctx.page.body += "\n\n" + page.body;
				delete page.body;

				merge(ctx.page, page);
				finish();
			});
		}
	}


	finish();
}