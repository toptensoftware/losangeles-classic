var url = require('url');
var merge = require('utils-merge');
var path = require('path');
var deepcopy = require('deepcopy');
var glob = require('glob');
var debug = require('debug')('losangeles:parsePage');

var isWin = /^win/i.test(process.platform);

/*
 * Loads a page from an on-disk file
 *
 * Tries all extensions registered in ctx.la.fileParsers.  Uses a default
 * filename of "index".
 * 
 * If file found, it's loaded, the file parser is called and the
 * loaded page is stored as ctx.page.
 * 
 * If ctx.pageDefaults.useFile is set the page is loaded from that file
 * 
 * If ctx.pageDefaults.fallbackFile is set that file will be used if no
 *     file matches the request url
 *
 * Files are located case insensitively and are globbed (so "/blog/post/001-*"
 * will find a page with slug).  If the located file doesn't match the requests
 * original url, a redirect is issued.  
 *
 * When loading from pageDefaults.useFile or pageDefaults.fallbackFile, 
 * redirects are never issued and the page.url is set to ctx.url (rather than
 * derived from the filename.
 *
 * After page is loaded it compares the ctx.method to page.httpMethod
 * only continues with the page if it matches (it it's a get request
 * and the page has no httpMethod setting.
 */
module.exports = function initParsePage(location)
{
	return function parsePageFilter(ctx, next)
	{
		// Page already loaded, or no file parsers installed?
		if (ctx.page || !ctx.la.fileParsers)
		{
			next();
			return;
		}

		// Work out base filename
		var filename;

		// True if should generate a redirect if the final filename doesn't
		// match the original url requested.
		var redirect = true;

		// See if pageDefaults specifies an explicit file to use
		if (ctx.pageDefaults && ctx.pageDefaults.useFile)
		{
			debug("using explicit file " + filename);
			filename = ctx.pageDefaults.useFile;
			redirect = false;
		}
		else
		{
			// Start with the path name
		 	filename = url.parse(ctx.url).pathname;

			// Index page?
			if (filename.substring(filename.length - 1)=="/")
			{
				redirect = false;
				filename += "index";
			}
			else
			{
				// If the filename part starts with a digit, include a glob
				// prefix to match leading zeros
				var parsedPath = path.parse(filename);
				var base = parsedPath.base;
				if (base[0]>='0' && base[0]<='9')
				{
					filename = path.join(parsedPath.dir, "*(0)" + removeLeadingZeros(base));
				}
			}
		
			// Build an array of all supported file extensions
			var exts = [];
			for (var key in ctx.la.fileParsers) 
			{
			    if (ctx.la.fileParsers.hasOwnProperty(key)) 
			    {
			    	exts.push(key);	
			    }
			}

			// No extensions
			if (exts.length==0)
			{
				next();
				return;
			}

			// Work out final glob pattern
			if (exts.length==1)
			{
				filename += exts;
			}
			else
			{
				filename += "@(" + exts.join("|") + ")";
			}

			// Prevent escaping the sandbox
			if (filename.indexOf("..")>=0)
				next();
		}

		// Prefix with our directory location directory	
		filename = path.join(location, filename);
		if (isWin)
			filename = filename.replace(/\\/g, "/");

		debug('globbing', filename);
		glob(filename, { "nocase": !isWin },  function globCallback(err, files) {

			debug("finished globbing", files);

			if (err)
				return next(err);

			if (files.length>1)
				return next(new Error("multiple files match glob pattern '" + filename.substring(location.length) + "'\n" + JSON.stringify(files)));

			if (files.length==0)
			{
				// Look for a fallback file
				if (ctx.pageDefaults && ctx.pageDefaults.fallbackFile)
				{
					redirect = false;
					parseFile(path.join(location, ctx.pageDefaults.fallbackFile))
					return;
				}

				return next();
			}

			// Load the file!
			parseFile(files[0]);
		});

		function parseFile(f)
		{
			debug("parsing file", f);

			// Work out url from filename
			var urlOfFile = stripExt(f.substring(location.length));

			// Remove leading zeros from public url
			var parsedUrl = path.parse(urlOfFile);
			urlOfFile = path.join(parsedUrl.dir, removeLeadingZeros(parsedUrl.base));

			if (isWin)
				urlOfFile = urlOfFile.replace(/\\/g, "/");

			// Find parser
			var parser = ctx.la.fileParsers[path.parse(f).ext];
			if (!parser)
			{
				debug("no suitable parser found");
				next();
				return;
			}

			// Call it
			parser(f, function fileParserCallback(err, page) {

				// Handle errors
				if (err)
				{
					if (err.code=="ENOENT")
					{
						debug("file not found", f);
						next();
					}
					else
					{
						debug("error parsing file", f, err);
						next(err);
					}
					return;
				}

				// Setup the page's url
				page.url = redirect ? urlOfFile : ctx.url;
				page.redirectOnUrlMismatch = redirect;

				// Check method
				var method = page.httpMethod || "get";
				if (!(method == "get" && ctx.method.toLowerCase() == "head") && (ctx.method.toLowerCase() != method.toLowerCase()))
				{
					debug("method mismatch", ctx.method, method);
					next();
					return;
				}

				// Done
				debug("page loaded", page);
				ctx.page = page;

				next();
			});
		}
	}
}

function stripExt(str)
{
	var p = path.parse(str);
	return path.join(p.dir, p.name);
}

function removeLeadingZeros(str)
{
	var count=0;
	while (count<str.length && str[count]=='0')
		count++;
	return str.substring(count);
}

