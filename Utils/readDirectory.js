var fs = require('fs');
var dir = require('node-dir');
var slash = require('slash');
var path = require('path');

// Enumerate all files in a folder
module.exports = function(rootFolder, folder, callback_err_files) 
{
	dir.files(path.join(rootFolder, folder), function(err, files) {

		// Quit if error
		if (err)
		{
			callback_err_files(err);
			return;
		}

		// Strip leading path
		files = files.map(function(item) {
			return item.substring(rootFolder.length);
		});

		callback_err_files(null, files);

	});

};
