
function isArray(x)
{
	return Object.prototype.toString.call( x ) === '[object Array]';
}

function isFunction(x)
{
	return typeof(x) === "function"
}

function isObject(x)
{
	return typeof(x) === "object"
}

// Create a filter that checks all conditions match
function createAndFilter(conditions)
{
	return function(subject)
	{
		for (var i=0; i<conditions.length; i++)
		{
			if (!conditions[i](subject))
				return false;
		}
		return true;
	}
}

// Create a filter that checks if one condition matches
function createOrFilter(conditions)
{
	return function(subject)
	{
		for (var i=0; i<conditions.length; i++)
		{
			if (conditions[i](subject))
				return true;
		}
		return false;
	}
}

function startsWith(subject, condition)
{
	return subject.length >= condition.length && 
			subject.substring(0, condition.length) === condition;
}

function endsWith(subject, condition)
{
	return subject.length >= condition.length && 
			subject.substring(subject.length - condition.length) === condition;
}

function startsWithAny(subject, condition)
{
	for (var i=0; i<condition.length; i++)
	{
		if (startsWith(subject, condition[i]))
			return true;
	}
	return false;
}

function endsWithAny(subject, condition)
{
	for (var i=0; i<condition.length; i++)
	{
		if (endsWith(subject, condition[i]))
			return true;
	}
	return false;
}

function bindFieldKey(fn, fieldKey)
{
	if (!fieldKey)
		return fn;

	if (fn.needsKey)
	{
		return function(subject) { return fn(subject, fieldKey); }
	}
	else
	{
		return function(subject) { return fn(subject[fieldKey]); }
	}
}

function combineFilterFunctions(conditions, orFlag, fieldKey)
{
	if (fieldKey)
	{
		for (var i=0; i<conditions.length; i++)
			conditions[i] = bindFieldKey(conditions[i], fieldKey);
	}

	if (conditions.length==1)
		return conditions[0];
	if (conditions.length==0)
		return function(subject) { return true; }

	if (orFlag)
	{
		return createOrFilter(conditions);
	}
	else
	{
		return createAndFilter(conditions);
	}
}

function createFilter(spec, orFlag, fieldKey)
{
	// Is it already a function?
	if (isFunction(spec))
	{
		return bindFieldKey(spec, fieldKey);
	}

	// Is it an array?
	if (isArray(spec))
	{
		var conditions = [];

		for (var i=0; i<spec.length; i++)
		{
			conditions.push(createFilter(spec[i]));
		}

		return combineFilterFunctions(conditions, orFlag, fieldKey);
	}

	// Regular expression?
	if (spec instanceof RegExp)
	{
		return bindFieldKey(function(subject) { return spec.test(subject); }, fieldKey);
	}

	// A Key/Value spec?
	if (isObject(spec))
	{
		var conditions = [];

		for (var key in spec)
		{
			var conditionSpec = spec[key];

			if (key[0]=='$')
			{
				switch (key)
				{
					case '$or':
					case '$in':
						conditions.push(createFilter(conditionSpec, true));
						break;
					case '$gt':
						conditions.push(function(subject) { return subject > conditionSpec; });
						break;
					case '$lt':
						conditions.push(function(subject) { return subject < conditionSpec; });
						break;
					case '$ge':
					case '$gte':
						conditions.push(function(subject) { return subject >= conditionSpec; });
						break;
					case '$le':
					case '$lte':
						conditions.push(function(subject) { return subject <= conditionSpec; });
						break;
					case '$ne':
						conditions.push(function(subject) { return subject !== conditionSpec; });
						break;
					case '$eq':
						conditions.push(function(subject) { return subject === conditionSpec; });
						break;

					case '$startsWith':
					case '$beginsWith':
						if (isArray(conditionSpec))
							conditions.push(function(subject) { return startsWithAny(subject, conditionSpec); });
						else
							conditions.push(function(subject) { return startsWith(subject, conditionSpec); });
						break;

					case '$endsWith':
					case '$finishesWith':
						if (isArray(conditionSpec))
							conditions.push(function(subject) { return endsWithAny(subject, conditionSpec); });
						else
							conditions.push(function(subject) { return endsWith(subject, conditionSpec); });
						break;

					case '$nin':
					{
						var fn = createFilter(conditionSpec, true);
						conditions.push(function(subject) { return !fn(subject); });
						break;
					}

					case '$not':
					{
						var fn = createFilter(conditionSpec, false);
						conditions.push(function(subject) { return !fn(subject); });
						break;
					}

					case '$size':
					case '$length':
					{
						conditions.push(function(subject) { return subject.length === conditionSpec; } );
						break;
					}

					case '$has':
					{
						var conditionSpecFn = createFilter(conditionSpec, false, null);

						conditions.push(function(subject) {
							for (var i=0; i<subject.length; i++)
							{
								if (conditionSpecFn(subject[i]))
									return true;
							}
							return false;
						});
						break;
					}

					case '$hasAll':
					{
						var conditionSpecFns = [];
						for (var i=0; i<conditionSpec.length; i++)
						{
							conditionSpecFns.push(createFilter(conditionSpec[i], false, null));
						}

						conditions.push(function(subject) {
							for (var i=0; i<conditionSpecFns.length; i++)
							{
								var found = false;
								for (var j=0; j<subject.length; j++)
								{
									if (conditionSpecFns[i](subject[j]))
									{
										found = true;
										break;
									}
								}
								if (!found)
									return false;
							}
							return true;
						});
						break;
					}

					case '$exists':
					{
						var fn = function(subject, key) { return key in subject === conditionSpec; };
						fn.needsKey = true;
						conditions.push(fn);
						break;
					}

					default:
						throw new Error("Unknown operator: '" + key + "'");
				}
			}
			else
			{
				conditions.push(createFilter(conditionSpec, false, key));
			}
		}

		return combineFilterFunctions(conditions, orFlag, fieldKey);
	}

	// Just plain comparison
	return bindFieldKey(function(subject) { return subject === spec; }, fieldKey);
}


function createCompositeComparator(comps)
{
	if (comps.length==1)
		return comps[0];

	return function(a,b) {

		for (var i=0; i<comps.length; i++)
		{
			var result = comps[i](a,b);
			if (result!=0)
				return result;	
		}

		return 0;
	};
}

function generic_cmp_asc(a, b)
{
	if (a<b)
		return -1;
	if (a>b)
		return 1;
	return 0;
}

function generic_cmp_desc(a, b)
{
	if (a<b)
		return 1;
	if (a>b)
		return 0;
	return 0;
}

function string_cmp_asc(a, b)
{
	return generic_cmp_asc(a.toString(), b.toString());
}

function string_cmp_desc(a, b)
{
	return generic_cmp_desc(a.toString(), b.toString());
}

function string_cmp_i_asc(a, b)
{
	return generic_cmp_asc(a.toLowerCase(), b.toLowerCase());
}

function string_cmp_i_desc(a, b)
{
	return generic_cmp_desc(a.toLowerCase(), b.toLowerCase());
}


function string_cmp_l_asc(a, b)
{
	return a.localeCompare(b);
}

function string_cmp_l_desc(a, b)
{
	return -a.localeCompare(b);
}

function string_cmp_li_asc(a, b)
{
	return a.toLowerCase().localeCompare(b.toLowerCase());
}

function string_cmp_li_desc(a, b)
{
	return -a.toLowerCase().localeCompare(b.toLowerCase());
}

function numeric_cmp_asc(a, b)
{
	return a-b;
}

function numeric_cmp_desc(a, b)
{
	return b-a;
}


// The idea here is to reduce all the parameters to a single function - rather than
// nested function (for performance)
function createDefaultComparator(ascending, caseSensitive, useLocal, numeric)
{
	if (numeric)
	{
		return ascending ? numeric_cmp_asc : numeric_cmp_desc;
	}

	if (useLocal)
	{
		if (caseSensitive)
		{
			return ascending ? string_cmp_li_asc : string_cmp_li_desc;
		}
		else
		{
			return ascending ? string_cmp_l_asc : string_cmp_l_desc;
		}
	}

	if (caseSensitive!==null && !caseSensitive)
		return ascending ? string_cmp_i_asc : string_cmp_i_desc;

	if (numeric===false)
		return ascending ? string_cmp_asc : string_cmp_desc;


	return ascending ? generic_cmp_asc : generic_cmp_desc;

}

function createComparator(spec)
{
	// If it's a function, use it direcly
	if (isFunction(spec))
		return spec;

	// If it's an array, treat it as a composite sort specification
	if (isArray(spec))
	{
		var comps = [];
		for (var i=0; i<spec.length; i++)
		{
			comps.push(createComparator(spec[i]));
		}
		return createCompositeComparator(comps);
	}

	// Object lets us use specs like:
	// { name: true, $caseSensitive: true, $useLocal: false }
	// { age: true, $numeric: true }
	if (isObject(spec))
	{
		var fieldName, ascending, caseSensitive = null, useLocal = false, numeric = null;
		var specialFields = false;

		for (var key in spec)
		{
			if (key[0]=='$')
			{
				specialFields = true;
				switch (key)
				{
					case "$caseSensitive":
						caseSensitive = spec[key];
						break;

					case "$useLocal":
						useLocal = spec[key];
						break;

					case "$numeric":
						numeric = spec[key];
						break;

					default:
						throw new Error("Unknown operator: '" + key + "'");
				}
			}
			else
			{
				if (fieldName)
					throw new Error("Multiple field names in sort specifier");

				fieldName = key;
				fieldSpec = spec[key];
			}
		}

		if (!fieldName)
			throw new Error("No field name in sort specifier")

		var comparator;
		if (isFunction(fieldSpec))
		{
			if (specialFields)
				throw new Error("Sort specifier includes callback and special fields");
			comparator = fieldSpec;
		}
		else
		{
			var ascending = fieldSpec==="asc"  || fieldSpec==="ascending" || fieldSpec===true;
			var descending = fieldSpec==="desc"  || fieldSpec==="descending" || fieldSpec===false;
			if (!ascending && !descending)
				throw new Error("Sort specifier doesn't include a valid asc/desc indicator");

			comparator = createDefaultComparator(ascending, caseSensitive, useLocal, numeric);
		}


		return function(a,b) { return comparator(a[fieldName], b[fieldName]); }
	}

	if (spec[0]=='-')
	{
		spec = spec.substring(1);

		// eg: "-name" or "-age"
		// (case sensitive, descending, not optimized for numerics)
		return function(a,b) { return generic_cmp_desc(a[spec], b[spec]); }
	}

	// eg: "name" or "age"
	// (case sensitive, ascending, not optimized for numerics)
	return function(a,b) { return generic_cmp_asc(a[spec], b[spec]); }
}

module.exports = {
	createFilter: createFilter,
	createComparator: createComparator,
	startsWith: startsWith,
	startsWithAny: startsWithAny,
	endsWith: endsWith,
	endsWithAny: endsWithAny,
};

