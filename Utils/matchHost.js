// Middleware to check req.hostname and invoke another middleware if it matches
module.exports = function(host, handler) 
{
	return function hostFilter(req, res, next) 
	{
		if (req.hostname.match(host))
			handler(req, res, next);
		else
			next();
	}
}
