var fs = require('fs');
var yaml = require('js-yaml');

/*
 * Parse a yaml page file
 * 
 * A yaml page is a text file with an optional blob of yaml data at the top
 * delimited by a string of characters.  Returns an object representing the yaml + 
 * the rest of the document stored in the "body" property.
 *
 * The string of characters can be any three of more consecutive characters.
 * 
 * Also, if the body contains just a line with three asterisks '***' everything
 * above the line will be extracted to 'summary' property on the returned object.
 * (handy for blog post summaries)
 * 
 * 		---
 * 		title: "Los Angeles"
 * 		view: "home"
 * 		---
 * 		Welcome to Los Angeles (summary)
 *
 *      ***
 * 
 *      body text
 *      body text
 *
 * Would be parsed to:
 *
 * 		{
 *          "filename": "filenamePassedToFunction.page"
 * 		    "title": "Los Angeles",
 * 		    "view": "home",
 * 		    "body": "Welcome to Los Angeles (summary)\n\n***\n\nbody text\nbody text\n",
 *          "summary": "Welcome to Los Angeles (summary)\n\n",
 * 		}
 */

module.exports = function parseYamlPage(filename, callback_err_page) 
{
	// Read the file
	fs.readFile(filename, 'utf8', function(err, data) 
	{
		// Error?
		if (err) 
		{
			callback_err_page(err);
			return;
		}

		// Setup the page object
		var page = {
			filename: filename,
		};

		// Count lead chars
		var leadchars = 1;
		while (leadchars < data.length && data[leadchars]==data[0]) 
		{
			leadchars++;
		}

		// Delimiter found?
		if (leadchars>=3) 
		{
			// Yes.

			var json = false;			
			var delim = data.substring(0, leadchars);
			if (delim[0]=='{')
			{
				delim = Array(delim.length).join('}');
				json = true;
			}
			var enddelim = data.indexOf(delim, leadchars);

			if (enddelim >= 0) 
			{
				var metadataText = data.substring(leadchars, enddelim);

				page.body = data.substring(enddelim + leadchars).trim();

				var metadata;
				try	
				{
					if (json)
					{
						var strJson = "({\n" + metadataText + "\n})";
						metadata = eval(strJson);
					}
					else
					{
						 metadata = yaml.load(metadataText);
					}
				}
				catch (err)	
				{
					callback_err_page(new Error("Error parsing page header (" + (json ? "json" : "yaml") + "): " + err.toString()));
					return;
				}

				for (key in metadata) 
				{
					page[key] = metadata[key];
				}
			}
			else
			{
				callback_err_page(new Error("Misformed attribute delimiters"));
				return;
			}
		}
		else
		{
			page.body = data;
		}

		// Split off the summary before "***"
		if (page.body)
		{
			var rx = /^\*\*\*$/m;

			var match = page.body.match(rx);
			if (match)
			{
				page.summary = page.body.substring(0, match.index);
			}
			else
			{
				page.summary = page.body;
			}
		}
		else
		{
			page.summary="";
		}

		callback_err_page(null, page);
	});
};
