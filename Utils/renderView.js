var merge = require('utils-merge');

// Provides a local version of render view with a configurable location
// property.  Basically a rip off of the standard express view method.
module.exports = function(options)
{
	var _viewCache = {};

	return function(req, viewName, viewOptions, callback_err_str)
	{
		var opts = {
			settings: {
				"views": options.location,
			},
		}

		// Merge config options
		if (options.options)
			merge(opts, options.options);

		// Merge view locals
		if (viewOptions._locals)
			merge(opts, viewOptions._locals);

		// Merge options
		if (viewOptions)
			merge(opts, viewOptions);

		// Look for cached view
		var view;
		if (opts.cache)
			view = _viewCache[viewName];

		// view
		if (!view) 
		{
			var app = req.app;

			// Resolve view + engine to view
			view = new (app.get('view'))(viewName, {
				defaultEngine: options.defaultEngine,
				root: options.location,
				engines: app.engines
			});

			// View found?
			if (!view.path) {
				var err = new Error('Failed to lookup view "' + viewName + '" in /' + options.location);
				err.view = view;
				throw err;
			}

			// prime the cache
			if (opts.cache) 
				_viewCache[viewName] = view;
		}

		// Render the view
		view.render(opts, callback_err_str);
	}

}