var merge = require('utils-merge');


function formatPageLink(page, totalPages)
{
	if (page==totalPages)
		return ".";
	else
		return "?page=" + page.toString();
}

/*
 * Builds an array of page links:
 */
function buildPagerLinks(currentPage, totalPages, opts)
{
	opts = merge({
		"classPreviousPage": "previous-page",
		"classNextPage": "next-page",
		"classDisabled": "disabled",
		"classPageNumber": "page-number",
		"classSelectedPageNumber": "selected",
		"textPreviousPage": "&#171; Previous",
		"textNextPage": "Next &#187",
	}, opts);

	pages = [];

	pages.push( {
		cssClass: opts.classPreviousPage + (currentPage > 1 ? "" : " " + opts.classDisabled),
		caption: opts.textPreviousPage,
		url: formatPageLink(currentPage-1, totalPages),
	});

	for (var i=1; i<=totalPages; i++)
	{
		pages.push( {
			cssClass: opts.classPageNumber + (i==currentPage ? " " + opts.classSelectedPageNumber : ""),
			caption: i.toString(),
			url: formatPageLink(i, totalPages),
		});
	}

	pages.push( {
		cssClass: opts.classNextPage + (currentPage < totalPages ? "" : " " + opts.classDisabled),
		caption: opts.textNextPage,
		url: formatPageLink(currentPage+1, totalPages),
	});

	return pages;
}


module.exports = buildPagerLinks;