var express = require('express');

module.exports = function Router()
{
	// Start with an express router
	var router = express.Router();

	// Attach self to request object so available during document processing
	router.use(function laSetup(req, res, next) {

		req.la = router;
		res.la = router;
		res.cacheAndSend = res.send;
		next();

	});

	// Add supports handler
	router.supports = function invokeFeature(handler)
	{
		handler(this)
	};


	// Collection of html transforms
	router.htmlTransforms = {};

	// Collection of file parsers (map of extension to file type)
	router.fileParsers = {};

	// Done
	return router;
}


