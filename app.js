var express = require('express');
var path = require('path');
var logger = require('morgan');
var la = require('./index');
var fs = require('fs');
var hbs = require('hbs');
var dateFormat = require('dateformat');
var swag = require('swag');

swag.registerHelpers(hbs.handlebars);

var app = express();

// view engine setup
app.use(logger('dev'));

hbs.registerHelper('toJSON', function(object){
  return JSON.stringify(object, null, 4);
});

hbs.registerHelper("formatLongDate", function(timestamp, format) {
  var fmt = arguments.length > 2 ? format : 'dddd, mmmm dS, yyyy';
  return dateFormat(new Date(timestamp), fmt);
});


hbs.registerHelper("groupByMonth", function (list, field, options) {

    var fn = options.fn;

    var groups = {};
    var result = [];
    for (var i=0; i<list.length; i++)
    {
        var item = list[i];
        var key = item[field].getYear() + " " + item[field].getMonth();

        if (!groups[key])
        {
          groups[key] = {
            value: item[field],
            items: [],
          }
          result.push(groups[key]);
        }
        
        groups[key].items.push(item);
    }


    var r =  result.reduce(function(buffer, item) {
        return buffer + fn(item);
      }, '');

    return r;
});

var contentRoot = process.env.LOSANGELES_CONTENT_ROOT || path.join(__dirname, "content");

var config = require(path.join(contentRoot, '/config'));

if (app.get('env')=="development")
  config.disableCache = true;

// Use the one an only site
app.use(new la.Server(config));


// catch 404 and forward to error handler
app.use(function notFoundMiddleware(req, res, next) {
  var err = new Error('Not Found - ' + req.originalUrl);
  err.status = 404;
  next(err);
});

// error handlers
app.use(function errorHandlerMiddleware(err, req, res, next) {
  console.log(err.message);
  res.status(err.status || 500);
  res.send(err.status + " - " + err.message);
});

module.exports = app;
