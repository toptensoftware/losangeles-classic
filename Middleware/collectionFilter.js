var filterAndSort = require('../Utils/filterAndSort');
var debug = require('debug')('losangeles:collectionFilter');

module.exports = function(req, res, next)
{
	if (!req.page || !req.page.filter)
		return next();

	// get the filter
	var filter = req.page.filter;

	// get the collection of items
	var items = req.page[filter.collection];

	debug("pre-filter items:", items.map(function(x) { return x.url }));

	// get filter callback
	var fn = filter.filter;
	if (typeof(fn) !== "function")
		fn = filterAndSort.createFilter(fn);

	// apply filter
	items = items.filter(function(item) {
		return fn(item, req);
	});

	debug("pre-filter items:", items.map(function(x) { return x.url }));

	req.page[filter.collection] = items;

	next();
}