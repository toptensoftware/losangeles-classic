var url = require('url');
var debug = require('debug')('losangeles:navigation');

/*
 * Generates meta data describing next and previous links for a page
 *
 * Page must have a "navigation" setting with a "collection" property identifying
 * the collection (another property on the page). Locates req.url in the collection and 
 * works out the next and previous page links.
 *
 * navigation.reverse can be used to flip the next/previous links
 *
 * eg:
 * ---
 * navigation:
 *     collection: "blogPosts"
 *     reverse: true
 *
 * After:
 * ---
 * navigation:
 *     collection: "blogPosts"
 *     reverse: true
 *     nextPage: { Object from collection }			// If any
 *     previousPage: { Object form collection} 		// If any
 * 
 */
module.exports = function navigationFilter(req, res, next)
{
	if (!req.page || !req.page.navigation)
	{
		next();
		return;
	}

	var navigation = req.page.navigation;
	var collectionName = navigation.collection || "items"

	// get the collection
	var items = req.page[collectionName];
	if (!items || !items.length)
	{
		next();
		return;
	}

	// Work out index of page we're currently on
	var indexThisPage = -1;
	var urlThisPage = url.parse(req.url).pathname;
	for (var i=0; i<items.length; i++)
	{
		if (items[i].url == urlThisPage)
			indexThisPage = i;
	}

	// Found?
	if (indexThisPage<0)
	{
		next();
		return;
	}

	debug('indexThis', indexThisPage);
	debug('pages', items.map(function(x) { return x.url }));

	// Next page
	if (indexThisPage>0)
	{
		navigation[navigation.reverse===true ? "nextPage" : "previousPage"] = items[indexThisPage-1];
	}

	// Previous page
	if (indexThisPage+1 < items.length)
	{
		navigation[navigation.reverse===true ? "previousPage" : "nextPage"] = items[indexThisPage+1];
	}

	debug('navigation', navigation);

	next();
}