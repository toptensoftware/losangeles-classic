/*
 * Renders the body a page with JSON content type as JSON
 */
 
module.exports = function renderJsonFilter(req, res, next) {

	if (req.page && req.page.contentType=="application/json")
	{
		res.json(req.body);
	}
	else
	{
		next();
	}

}