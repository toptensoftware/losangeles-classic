/*
 * Provides simple url redirect and rewrite facilities
 *
 * Expects an array of rule objects where each rule contains:
 * 
 * Either "redirect" or "rewrite" property which is a regex
 * A "to" property which is the new url.
 *
 * eg:
 * [
 *    { redirect: /^\/rss.xml$/i, to: "/blog/feed" },
 * ]
 * 
 * A blank "to" string invokes a 404 and can be used to hide files.
 * 
 * [
 *    { rewrite: /^\/config\.js.*$/i, to: "" },
 *    { rewrite: /^\/views/i, to: "" },
 * ]
 */

var debug = require('debug')('losangeles:urlRules');

module.exports = function initUrlRules(rules) 
{
	return function urlRulesFilter(req, res, next)
	{
		if (!rules)
		{
			next();
			return;
		}

		for (var i=0; i<rules.length; i++)
		{
			var rule = rules[i];

			var rx = rule.redirect ? rule.redirect : rule.rewrite;

			// If the rule contains "://" also include the protocol and hostname is the test string
			if (rule.redirect && rx.toString().indexOf(":\\/\\/")>=0)
			{
				testUrl = req.protocol + "://" + req.headers.host + req.url;
			}
			else
			{
				testUrl = req.url;
			}

			var newUrl = testUrl.replace(rx, rule.to);
			debug("matching", testUrl, "vs", rx, "gives", newUrl);
			if (newUrl!==testUrl)
			{
				if (newUrl=="")
				{
					var err = new Error('Not Found - ' + req.originalUrl);
					err.status = 404;
					next(err);
					return;
				}
				else
				{
					if (rule.redirect)
					{
						res.redirect(newUrl);
						return;
					}

					req.url = newUrl;
					break;
				}
			}
		}
		next();
	}

}