var path = require('path');
var cp = require('child_process');

/*
 * Executes an external process
 * 
 * Looks for page.exec and if found executes page.exec.cmd in the
 * the folder page.exec.dir.
 *
 * The output of the executed command is appended to the page.body
 * 
 * Can be used for "git pull" to pull latest content from a remote repo.
 * Recommend setting method to "post".
 *
 * eg: yaml header:
 * ---
 * httpMethod: post
 * cache: "clear"
 * exec:
 *     cmd: git pull
 *     dir: .
 */
module.exports = function execFilter(req, res, next) 
{
	// Quit if no page loaded, or no exec setting
	if (!req.page || !req.page.exec)
	{
		next();
		return;
	}

	// Work out folder to execute in
	var exec_dir = path.dirname(req.page.filename);
	if (req.page.exec.dir)
		exec_dir = path.join(exec_dir, req.page.exec.dir);

	// Setup exec options
	var exec_options = 
	{
		"cwd": exec_dir,
	};

	// Execute it
	cp.exec(req.page.exec.cmd, exec_options, function execCallback(err, stdout, stderr) 
	{
		// Handle error
		if (err)
		{
			next(err)
			return;
		}

		// Append output to the page body
		req.page.body += stdout + stderr;

		// Done
		next();
	});


}