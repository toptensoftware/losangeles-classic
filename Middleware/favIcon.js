var path = require('path');
var fs = require('fs');

/*
 * favicon handler
 *
 * Looks for location + "favicon.ico".
 * If found, installs the standard express favicon middleware
 * If not found, installs a dummy pass through middleware
 */
module.exports = function initFavIcon(location)
{
	// Work out file location
	var faviconFile = path.join(location, 'favicon.ico');

	// Exists?
	if (fs.existsSync(faviconFile))
	{
		return require('serve-favicon')(faviconFile);
	}
	else
	{
		return function nopFavIcon(req, res, next) { next(); }
	}
}