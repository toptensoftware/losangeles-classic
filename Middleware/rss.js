var url = require('url');
var rssGenerator = require('rss');
var debug = require('debug')('losangeles:rss');

/*
 * Generates an rss feed for a page with rssFeed properties
 *
 * Expects:
 * ---
 * blogPosts: [ ... rss feed items ... ]
 * rssFeed:
 *     collection: "blogPosts"		// collect to get feed items from
 *     includeFullArticle: false	// use page.body or page.summary?
 * 
 * plus properties to pass to rssGenerator. eg:
 *
 *     title: "Los Angeles Test Site"
 *     description: "Demo of how to setup a Los Angeles server"
 *     managingEditor: "Topten Software"
 *     copyright: "Copyright © 2008-2015 Topten Software.  All Rights Reserved"
 *     ttl: 720
 *
 * After processing, body will be updated with generate XML feed in text and page
 * contentType is changed to text/xml.
 */
module.exports = function rssFilter(req, res, next)
{
	// Is it an rss page?
	if (!req.page || !req.page.rssFeed)
	{
		next();
		return;
	}

	var rssSettings = req.page.rssFeed;
	var collectionName = rssSettings.collection || "items"

	debug(req.page);

	// get the collection
	var items = req.page[collectionName];
	if (!items)
	{
		next();
		return;
	}

	// Fill in missing defaults
	if (!rssSettings.feed_url) {
		rssSettings.feed_url = req.protocol + "://" + req.headers.host + url.parse(req.url).pathname;
	}
	if (!rssSettings.site_url) {
		rssSettings.site_url = req.protocol + "://" + req.headers.host;
	}
	if (rssSettings.copyright)
	{
		rssSettings.copyright = rssSettings.copyright.replace("$(year)", (new Date()).getFullYear().toString());
	}

	// Generate rss
	var rss = new rssGenerator(rssSettings);
	for (var i=0; i<items.length; i++)
	{
		var item = items[i];

		// Work out fully qualified URL
		var fullUrl = req.protocol + "://" + req.headers.host + item.url;

		// Get body
		var body = rssSettings.includeFullArticle ? item.body : item.summary;

		// Transform it
		if (req.la.htmlTransforms[item.contentType])
		{
			body = req.la.htmlTransforms[item.contentType](req, body, { 
				fullyQualified: true,
				location: item.url,
			});
		}

		// Add the continue reading link
		body += "\n<p><a href='" + fullUrl + "'>Continue reading...</a></p>\n\n";

		// Add item
		rss.item({
			title: item.title,
			description: body,
			url: fullUrl,
			date: item._date,
			categories: item.tags,
		});
	}

	// Generate the page body
	req.page.body = rss.xml({indent: true});
	req.page.contentType = "text/xml";
	delete req.page.view;
	next();
}