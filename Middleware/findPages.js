var filterAndSort = require('../Utils/filterAndSort');
var debug = require('debug')('losangeles:findPages');

/*
 * findPages - finds and parses a set of files given a file spec
 *
 * Looks for req.page.findPages with the following properties
 * 
 * - filespec - file spec to be globbed
 * - sort - a sort expression to sort by 
 * - name - where to store the result on the page.
 */

module.exports = function findPagesFilter(req, res, next)
{
	if (!req.page || !req.page.findPages)
		return next();

	var fp = req.page.findPages;

	debug("scanning folder", fp);
	req.la.findPages(fp.filespec, function(err, pages) {

		debug("folder scan finished", pages);

		if (fp.filter)
		{
			pages = pages.filter(filterAndSort.createFilter(fp.filter));
		}

		if (fp.sort)
		{
			pages = pages.slice(0, pages.length);
			pages.sort(filterAndSort.createComparator(fp.sort));
		}

		if (!err)
		{
			req.page[fp.name] = pages;
		}

		next();
	});

}