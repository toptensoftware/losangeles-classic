/*
 * Attempts to transform any page body to html
 *
 * - contentType mustn't already be text/html
 * - looks for a transformation function in req.la.htmlTransforms
 *      using the req.contentType as the look up
 * - after transform, req.contentType is set to text/html
 */ 

module.exports = function transformToHtmlFilter(req, res, next)
{
	if (req.page && req.page.contentType!="text/html" && req.la.htmlTransforms[req.page.contentType])
	{
		if (req.page.body)
			req.page.body = req.la.htmlTransforms[req.page.contentType](req, req.page.body, {});
		if (req.page.summary)
			req.page.summary = req.la.htmlTransforms[req.page.contentType](req, req.page.summary, {});
		req.page.contentType = "text/html";
	}

	next();
}
