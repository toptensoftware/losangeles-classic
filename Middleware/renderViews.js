/*
 * Processes a page by rendering it via a view engine
 * 
 * Checks the page for a "view" property.
 * 
 * If found, calls req.la.renderView passing the view, and the page body
 * Stores the rendered view back to page.body and updates the content type
 * to either "text/html" or page.finalContentType if specified
 *
 * Note that the default yaml page loader sets view to "page" but can be
 * overridden in the page's yaml header.
 */


module.exports = function transformViewsFilter(req, res, next)
{
	// Must have a page with a view property
	if (!req.page || !req.page.view || !req.la.renderView)
	{
		next();
		return;
	}

	// Render the page
	req.la.renderView(req, req.page.view, req.page, function renderViewCallback(err, str) {

		// Handle errors
		if (err)
		{
			next(err);
			return
		}

		// Replace page body with rendered content
		req.page.body = str;
		req.page.contentType = req.page.finalContentType || "text/html";

		// Remove the view attribute since it's been handled
		delete req.page.view;

		// Continue
		next();
	});
}
