/*

Creates content experiments on a page:

eg:

---
experiment:
    name: "Download Button Color"
    variations:
    - 
      name: red
      weight: 50
      page:
        title: "Variation A"
    - 
      name: blue
      weight: 50
      page:
        title: "Variatino B"
---

{{#if red}}
The Red Pill
{{/if}}

{{#if blue}}
The Blue Pill
{{/if}}


Also sets up page.experimentVariant and page.experimentName that can be used in the layout to pass the
selected experiment to analytics tracking as custom variables

eg:

	  _gaq.push(['_setAccount', 'UA-XXXXXXX-X']);
{{#if experimentVariant}}
	  _gaq.push(['_setCustomVar', 1, 'Experiment Variant', '{{{experimentVariant}}}']);
{{/if}}
{{#if experimentName}}
	  _gaq.push(['_setCustomVar', 2, 'Experiment Name', '{{{experimentName}}}']);
{{/if}}
	  _gaq.push(['_trackPageview']);

*/


var crc = require('crc-32');
var handlebars = require('handlebars');
var merge = require('utils-merge');

module.exports = function contentExperiments(req, res, next)
{
	if (req.page && req.page.experiment2)
		console.log(req.page.experiment2);

	if (req.page && req.page.experiment)
	{
		// Get the experiments array
		var experiment = req.page.experiment;

		// Get the variations, create a default "A"/"B" 50/50 variation otherwise
		var variations = experiment.variations;
		if (!variations)
		{
			variations = [ {}, {} ];
		}

		// Initialize defaults
		var totalWeight = 0;
		for (var i=0; i<variations.length; i++)
		{
			if (!variations[i].name)
				variations[i].name = String.fromCharCode(65 + i);
			if (!variations[i].weight)
				variations[i].weight = 1;
			totalWeight += variations[i].weight;
		}

		// Read the user's variation key from cookie
		var keyVal;
		if (req.cookies)
		{
			var keyVal = parseInt(req.cookies["laExperimentKey"]);
			if (!keyVal)
			{
				while (!keyVal)
				{
					keyVal = parseInt(Math.random() * 1000000)
				}

				res.cookie("laExperimentKey", keyVal);
			}
		}
		else
		{
			var key = "losangles-contentExperiment-salt" + req.connection.remoteAddress + "//" + req.path;
			keyVal = Math.abs(crc.str(key));
		}


		// Work out which variation based on key an weights
		var keyIndex = keyVal % totalWeight;
		var variation;
		for (variation=0; variation<variations.length; variation++)
		{
			if (keyIndex < variations[variation].weight)
				break;
			keyIndex -= variations[variation].weight;
		}

		// Setup handlebar render options
		var opts = {
			variation: variations[variation].name,
		};
		for (var i=0; i<variations.length; i++)
		{
			opts[variations[i].name] = i==variation;
		}

		if (variations[variation].params)
			merge(opts, variations[variation].params);

		// Create the template (or get it from the cache)
		var template;
		var cache = req.la.cache;
		if (cache)
		{
			var cacheKey = req.protocol + "://" + req.headers.host + req.originalUrl;

			if (!cache.contentExperimentTemplates)
				cache.contentExperimentTemplates = {};

			template = cache.contentExperimentTemplates[cacheKey];

			if (!template)
			{
				template = handlebars.compile(req.page.body);
				cache.contentExperimentTemplates[cacheKey] = template;
			}
		}
		else
		{
			template = handlebars.compile(req.page.body);
		}

		// Render the new body
		req.page.body = template(opts);

		// Include variation details
		req.page.experimentVariant = variations[variation].name;
		req.page.experimentName = experiment.name || "Unnamed Experiment";

		// Disable caching otherwise everyone will get the same paeg
		req.page.cache = false;

		// Merge variation parameters back onto the page (eg: title experiments)
		if (variations[variation].page)
		{
			merge(req.page, variations[variation].page);
		}

	}

	next();
}
