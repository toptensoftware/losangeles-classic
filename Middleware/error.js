/*
 * Filter to generate error page
 *
 * Includes stack track if development environment.
 * Error is also logged to console.
 */

module.exports = function errorFilter(err, req, res, next)
{
	// Log it
    console.log(err.message);

    // Return error status
    res.status(err.status || 500);

    // Render the error page
    req.la.renderView(req, 'error', 
	    {
	      message: err.message,
	      error: req.app.get('env')==="development" ? err : {},
	      req: req
	    }, 
	    function renderViewCallback(err, str) {

	    	// Send it
	    	if (err)
	    	{
	    		res.send(err.message);
	    	}
	    	else
	    	{
				res.set('Content-Type', "text/html");
	    		res.send(str);
	    	}
	    }
	);

}