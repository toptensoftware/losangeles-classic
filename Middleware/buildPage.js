
module.exports = function(req, res, next)
{
	var ctx = {
		url: req.url,
		originalUrl: req.originalUrl,
		method: req.method,
		query: req.query,
	};

	req.la.buildPage(ctx, function buildPageCallback(err) {
		if (err)
			return next(err);

		if (ctx.page)
		{
			req.page = ctx.page;

			// Preserve the query string parameters even if redirecting
			var baseUrl = req.originalUrl;
			var q = req.originalUrl.indexOf('?');
			if (q>=0)
			{
				baseUrl = req.originalUrl.substring(0, q);
				q = req.originalUrl.substring(q);
			}
			else
			{
				q = "";
			}


			if (req.page.redirectOnUrlMismatch && req.page.url!=baseUrl)
			{
				res.redirect(req.page.url + q);
				return;
			}
		}
		next();
	});
}