var buildPagerLinks = require('../Utils/buildPagerLinks');
var debug = require('debug')('losangeles:pagination');

// Give a number of items and a page size, work out how many pages
function pageCount(count, pageSize) 
{
	if (count==0)
		return 1;
	return Math.floor((count-1)/pageSize) + 1;
}

// Skip take from an array
function take(arr, skip, take) 
{
	return arr.slice(skip, skip + take); 
}

function takePage(arr, pageNumber, pageSize) 
{
	return take(arr, pageNumber * pageSize, pageSize);
}

/*
 * Extracts a page of items from a collection on a page
 *
 * Looks for req.query.page to decide the current page number
 * 
 * Page must have:

 * ---
 * blogPosts: [ ... ]
 * pagination:
 *     collection: "blogPosts"			// Name of the collection to be paginated
 *     pageSize: 20						// Number of items per page
 * ---
 *
 * and perhaps options to pass to buildPageLinks. eg: 
 * 
 *     textNextPage: "Newer &#187"	
 *     textPreviousPage: "&#171; Older"
 *
 * After processing:
 *
 * blogPosts: [ .. clipped to current page .. ] 
 * pagination:
 *     collection: "blogPosts"		// Unchanged
 *     pageSize: 20					// Unchanged
 *     pageNumber: 1				// Current page number
 *     pageCount: 5					// Number of pages
 *     pagerLinks: [ ] 				// An array of pager links for building pagination
 */
module.exports = function paginationFilter(req, res, next)
{
	if (!req.page || !req.page.pagination)
	{
		next();
		return;
	}

	var pagination = req.page.pagination;
	var collectionName = pagination.collection || "items"

	// get the collection
	var items = req.page[collectionName];
	if (!items)
	{
		next();
		return;
	}


	// Work out the page count
	pagination.pageCount = pageCount(items.length, pagination.pageSize);

	// Public page numbers 1 = oldest N = newest
	var displayPage = req.query.page;
	if (!displayPage)
		displayPage = pagination.pageCount;
	else
		displayPage = parseInt(displayPage);

	// Internal page numbers 0 = newest N-1 = oldest
	var page = pagination.reverse ? pagination.pageCount - displayPage : displayPage;

	// Add data
	pagination.pageNumber = displayPage;
	pagination.pagerLinks = buildPagerLinks(pagination.pageNumber, pagination.pageCount, pagination);

	// Trim the collection to the request page
	req.page[collectionName] = takePage(items, page, pagination.pageSize);

	debug("paginating", pagination);

	// Done
	next();
}