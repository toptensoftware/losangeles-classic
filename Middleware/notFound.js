/*
 * Generates a 404 error if nothing handles request
 */

module.exports = function notFoundFilter(req, res, next)
{
	var err = new Error('Not found - ' + req.originalUrl);
	err.status = 404;
	next(err);
}