/*
 * Renders any contentType "text/*" by sending it to the response
 */

module.exports = function renderTextFilter(req, res, next) {

	if (req.page && req.page.contentType && req.page.contentType.substring(0,5)=="text/")
	{
		res.set('Content-Type', req.page.contentType);
		res.cacheAndSend(req.page.body);
	}
	else
	{
		next();
	}

}