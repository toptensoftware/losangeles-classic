var express = require('express');
var path = require('path');
var logger = require('morgan');
var la = require('LosAngeles');

var app = express();

// view engine setup
app.use(logger('dev'));

// Setup LosAngeles page server
app.use(new la.Server(require('./content/config')));


// catch 404 and forward to error handler
app.use(function notFoundMiddleware(req, res, next) {
  var err = new Error('Not Found - ' + req.originalUrl);
  err.status = 404;
  next(err);
});

// error handlers
app.use(function errorHandlerMiddleware(err, req, res, next) {
  console.log(err.message);
  res.status(err.status || 500);
  res.send(err.status + " - " + err.message);
});

module.exports = app;
