var hbs = require('hbs');

hbs.registerHelper('toJSON', function(object){
	return JSON.stringify(object, null, 4);
});

module.exports = 
{
  host: /la-test\./,
  location: __dirname,

  pageDefaults:
  [
    // In the blog folder, populate any pages with a "blogPosts" collection
    {
      urlPrefix: "/blog/",
      data:
      {
        findPages:
        {
          name: "blogPosts",
          filespec: "/blog/posts/*.page",
          sort: "-url",
          filter: function(item) { return !item.draft }
        },
      }
    },

    // For blog post pages use the "blogPost" view template
    // and also enable inter-page navigation
    {
      urlPrefix: "/blog/posts/",
      data:
      {
        view: "blogPost",
        navigation: 
        {
          collection: "blogPosts",
          reverse: true,
        },
      }
    },

    // The tag pages have a default handler page (so you don't need to create
    // a page for every tag).  Also the blog posts collection is filtered by
    // by the selected tag
    {
      urlMatch: "/blog/tags/:tag",
      data:
      {
        view: "blogIndex",
        fallbackFile: "/blog/tags/default.page",
        filter:
        {
          collection: "blogPosts",
          filter: function(item, req) { return item.tags && item.tags.indexOf(req.page.tag)>=0; }
        }
      }
    }

  ],

  urlRules: 
  [
    { redirect: /^\/Feed$/i, to: "/blog/feed" },
    { rewrite: /^\/config\.js.*$/i, to: "" },
    { rewrite: /^\/views/i, to: "" },
  ],
}