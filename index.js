var merge = require('utils-merge');

module.exports = merge(

	// Core API
	require('./api'), 

	// Other stuff
	{
		Server: require('./Server'),
	}
);
