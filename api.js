var merge = require('utils-merge');

module.exports = {

	filters: {
		parsePage: require('./Filters/parsePage'),
		pageDefaults: require('./Filters/pageDefaults'),
		mergePageDefaults: require('./Filters/mergePageDefaults'),
		mergeOtherPages: require('./Filters/mergeOtherPages'),
	},

	middleware: {
		exec: require('./Middleware/exec'),
		findPages: require('./Middleware/findPages'),
		collectionFilter: require('./Middleware/collectionFilter'),
		pagination: require('./Middleware/pagination'),
		navigation: require('./Middleware/navigation'),
		contentExperiments: require('./Middleware/contentExperiments'),
		transformToHtml: require('./Middleware/transformToHtml'),
		
		urlRules: require('./Middleware/urlRules'),
		favIcon: require('./Middleware/favIcon'),
		buildPage: require('./Middleware/buildPage'),
		renderViews: require('./Middleware/renderViews'),
		renderText: require('./Middleware/renderText'),
		renderJson: require('./Middleware/renderJson'),
		rss: require('./Middleware/rss'),
		notFound: require('./Middleware/notFound'),
		error: require('./Middleware/error'),
	},

	features: {
		cache: require('./Features/cache'),
		imageSize: require('./Features/imageSize'),
		markdown: require('./Features/markdown'),
		viewEngine: require('./Features/viewEngine'),
		yamlPages: require('./Features/yamlPages'),
		findPages: require('./Features/findPages'),
		pageCore: require('./Features/pageCore'),
	},

	utils: merge({
		buildPagerLinks: require('./Utils/buildPagerLinks'),
		matchHost: require('./Utils/matchHost'),
		parseYamlPage: require('./Utils/parseYamlPage'),
		readDirectory: require('./Utils/readDirectory'),
		renderView: require('./Utils/renderView'),
	}, require('./Utils/filterAndSort')),

	Router: require('./Router.js'),

};