# LosAngeles.js

LosAngeles.js provides a set of tools for implementing a mostly static content page server.

LosAngeles.js leverages Express' router object to build up a page through a series of load, transform
and render operations.  Typically the content is then cached and never generated again (until either
the server is restarted, or the cache is deliberately flushed).

Although somewhat similar to a static site generator in many respects, it's not really designed
for that purpose.  It's designed for small to medium size sites with a reasonable number of static
pages. Also useful for sites that have some dynamic content -  LosAngeles provides the static
content generation, while you can build the dynamic parts in regular node.

## What's With the Name?

LosAngeles.js is named after my favourite track at the time when I started developing it - "Los Angeles" 
by "The Midnight".  It just so happened to be playing when I needed a name for this project.

## Features

* Highly opinionated static content engine
* By default, content written in Markdown, layout and rendering with Handlebars
* Built in caching support saves regenerating pages on each request
* Simple text page support - yaml and the top, markdown at the bottom
* Automatically determine image sizes when transforming Markdown
* Ability to execute external programs (eg: use git hooks to invoke a git pull to grab new content)
* Most content automatically cached (but cache control to flush it)
* Pagination of items (eg: blog post index)
* Navigation between items (eg: next/previous blog post)
* Support for RSS generation
* View Engine support (handlebars by default)
* Multi-site support - serve multiple domains from one server
* Use the built-in pre-canned transformation stack, or build your own from parts.



