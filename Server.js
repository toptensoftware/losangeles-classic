var express = require('express');
var la = require('./api');
var fs = require('fs');
var path = require('path');

module.exports = function Server(config)
{
	var router = la.Router();

	// Install features
	if (!config.disableCache)
		router.supports(la.features.cache);
	else
		console.log("Cache disabled")
	
	router.supports(la.features.markdown);
	router.supports(la.features.imageSize(config.location));
	router.supports(la.features.viewEngine(config.location, config.views));
	router.supports(la.features.yamlPages);
	router.supports(la.features.findPages(config.location));
	router.supports(la.features.pageCore);

	// URL redirect and rewrite rules
	router.use(la.middleware.urlRules(config.urlRules));

	// Static content
	router.use(la.middleware.favIcon(config.location));
	router.use(express.static(config.location, { maxAge: "1d" }));

	// Page core
	router.useFilter(la.filters.pageDefaults(config.pageDefaults));
	router.useFilter(la.filters.parsePage(config.location));
	router.useFilter(la.filters.mergePageDefaults);
	router.useFilter(la.filters.mergeOtherPages);
	router.use(la.middleware.buildPage);

	// Page transforms
	router.use(la.middleware.contentExperiments);
	router.use(la.middleware.findPages);
	router.use(la.middleware.exec);
	router.use(la.middleware.collectionFilter);
	router.use(la.middleware.pagination);
	router.use(la.middleware.navigation);
	router.use(la.middleware.transformToHtml);

	// Renderers
	router.use(la.middleware.rss);
	router.use(la.middleware.renderViews);
	router.use(la.middleware.renderText);
	router.use(la.middleware.renderJson);

	// Error handling
	router.use(la.middleware.notFound);
	router.use(la.middleware.error);

	// If config includes a "host" condition, then filter
	// the entire server by it
	if (config.host)
	{
		console.log(config.host);
		return la.utils.matchHost(config.host, router);
	}
	else
		return router;
}