var path = require('path');
var async = require('async');
var merge = require('utils-merge');
var readDirectory = require('../Utils/readDirectory');
var glob = require('glob');
var debug = require('debug')('losangeles:findPagesFeature');

function stripExtension(str)
{
	var p = path.parse(str);
	return path.join(p.dir, p.name);
}

/*
 * Installs support for enumerating meta data of files in a folder 
 * 
 * Only files with a file parser registered in la.fileParsers
 * are loaded.  File is loaded and parsed to extract meta data.
 *
 * Available as req.la.findPages(folder, callback);
 */
module.exports = function initFindPages(location)
{
	var inProgress = {};

	return function installFindPages(la)
	{
		la.findPages = function findPages(folder, callback_err_files)
		{
			// Check if cached
			if (la.cache)
			{
				if (!la.cache.globbedFolders)
					la.cache.globbedFolders = {};

				var files = la.cache.globbedFolders[folder];
				if (files)
				{
					debug("found in cache", folder);
					return callback_err_files(null, files);
				}
			}

			if (inProgress[folder])
			{
				callback_err_files(null, inProgress[folder]);
				return;
			}

			debug("scanning", folder);
			glob(path.join(location, folder), function globCallback(err, files) {

				// Quit if error
				if (err)
				{
					callback_err_files(err);
					return;
				}

				debug("Found", files);

				// Convert file name strings to an array of objects
				files = files.map(function mapFiles(x) {
					x = x.substring(location.length);
					return { 
						url: stripExtension(x),
					};
				});

				inProgress[folder] = files;

				// Read meta data from all files
				var walker = new async.queue(function asyncQueueCallback(x, callback) {

					debug("building page", x);
					la.buildPage(x, function(err) {

						if (x.page)
						{
							// Transform the body and summary
							if (la.htmlTransforms[x.page.contentType])
							{
								x.page.body = la.htmlTransforms[x.page.contentType](null, x.page.body, { 
									location: x.page.url,
								});
								x.page.summary = la.htmlTransforms[x.page.contentType](null, x.page.summary, { 
									location: x.page.url,
								});
							}
						}
						callback(err);
					});
					//callback(null);

					/*
					var parser = la.fileParsers[path.parse(x.filename).ext];
					if (parser)
					{
						parser(path.join(location, x.filename), function fileParserCallback(err, page) {
							if (!err)
							{
								// Merge page existing object
								merge(x, page)


							}

							callback(null);
						});
					}
					else
						callback(null);
					*/

				}, 50);

				walker.push(files);
				walker.drain = function asyncOnDrain(err) {

					files = files.map(function(x) { return x.page; })

					if (la.cache)
						la.cache.globbedFolders[folder] = files;

					delete inProgress[folder];

					debug("finished", folder);
					callback_err_files(null, files);
				};

			});
		}
	}
}