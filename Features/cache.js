var url = require('url');

function cacheKey(req)
{
	return req.protocol + "://" + req.headers.host + req.originalUrl;
}

// res.cacheAndSend
function cacheAndSend(body)
{
	var req = this.req;

	// Is caching enabled?
	var cache = req.la.cache;
	if (cache && req.page)
	{
		// Is caching allowed on this page?
		if (req.page.cache===true || req.page.cache===undefined)
		{
			if (!cache.pages)
				cache.pages = [];

			cache.pages[cacheKey(req)] = {
				contentType: this.get('Content-Type'),
				body: body,
			};
		}
	}

	if (req.method.toLowerCase() == "head")
		this.send("");
	else
		this.send(body);
}

module.exports = function installCache(la)
{
	la.cache = {};

	la.use(function cacheFilter(req, res, next) {

		// Check cache is setup, has a pages collection and this is a GET request
		if (req.la.cache && req.la.cache.pages && (req.method.toLowerCase()=="get" || req.method.toLowerCase() == "head"))
		{
			// Look up url
			var cachedResponse = req.la.cache.pages[cacheKey(req)];
			if (cachedResponse)
			{
				// Send cached response
				res.set('Content-Type', cachedResponse.contentType);
				if (req.method.toLowerCase() == "head")
					res.send("");
				else
					res.send(cachedResponse.body);
				return;
			}
		}

		// Install response end handler to check for clear cache flag on page
		req.on("end", function cacheOnEnd() 
		{
			if (req.la.cache && req.page && req.page.cache === "clear")
			{
				req.la.cache = {};
			}
	    });

		// Install cache and send method
		res.cacheAndSend = cacheAndSend;
		next();
	});
}