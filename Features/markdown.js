var url = require('url');
var path = require('path');


/* 
 * Installs support for markdown using MarkdownDeep
 * Registers a HTML transform for "text/markdown"
 */
module.exports = function installMarkdown(la)
{
	var MarkdownDeep = require('markdowndeep');
	var mdd = new MarkdownDeep.Markdown();
	mdd.ExtraMode = true;
	mdd.SafeMode = false;
	mdd.MarkdownInHtml = false;
	mdd.AutoHeadingIDs = true;
	mdd.UserBreaks = true;
	mdd.HtmlClassTitledImages = "figure";

	// Install markdown transform
	/*
	 * @param {Object} req - express request object
	 * @param {Object} options
	 * @param {String} options.location - location of the file being rendered
	 * @param {Boolean} options.fullyQualified - fully qualifies embedded links and urls (for rss)
	 * @returns {String} the transformed HTML text
	 */
 	la.htmlTransforms["text/markdown"] = function transformMarkdown(req, text, options)
	{
		// Work out location of the content being served
		var location = options.location;
		if (!location)
			location = url.parse(req.url).pathname;
		if (location.substr(-1)!='/')
			location = path.dirname(location);

		// Is the image size feature enabled?
		if (la.getImageSize)
		{
			mdd.OnGetImageSize= function onGetImageSize(src) 
			{
				// Fully qualified, can't do it
				if (src.indexOf('://')>=0)
					return undefined;

				// relative url?
				if (src.substring(0, 1)!='/')
				{
					src = path.join(location, src);
				}

				// Get the image size
				return la.getImageSize(src);
			};
		}

		// Setup base and root locations to qualify urls
		if (options.fullyQualified)
		{
			mdd.UrlRootLocation = req.protocol + "://" + req.headers.host;
			mdd.UrlBaseLocation = mdd.UrlRootLocation + location;
		}
		else
		{
			mdd.UrlBaseLocation = location;
		}

		// Do the transformation
		var result = mdd.Transform(text);

		// Clean up
		mdd.OnGetImageSize = null;

		// Done
		return result;
	}

}