var path = require('path');
var readImageSize = require('image-size');

/*
 * Installs la.getImageSize method
 *
 * Currently implemented as a synchronous read of the file size
 * but the size is then cached (if caching is enabled)
 *
 * (MarkdownDeep doesn't support resolving image sizes asynchronously
 *  so we need this to work syncrhonously)
 *
 * `location` parameter indicates root location of urls so files
 * can be located.
 *
 */


module.exports = function initImageSize(location) {

	return function installImageSize(la)
	{
		la.getImageSize = function getImageSize(pathname) {
			return getImageSizeHelper(la.cache, path.join(location, pathname));
		}
	}

}

function getImageSizeHelper(cache, filename) {

	// Look up cache
	if (cache)
	{
		if (!cache.imageSizes)
			cache.imageSizes = {};

		if (filename in cache.imageSizes)
		{
			return cache.imageSizes[filename].imageSize;
		}
	}

	// Read image size
	var imageSize;
	try
	{
		imageSize = readImageSize(filename);

		// Adjust for retina images
		if (filename.substr(-7).toLowerCase()=="@2x.png")
		{
			imageSize.width /= 2;
			imageSize.height /= 2;
		}
	}
	catch (e)
	{
	}

	// Cache it (if cache enabled)
	if (cache)
	{
		cache.imageSizes[filename] = {
			imageSize: imageSize,
		};
	}

	return imageSize;
}



