var merge = require('utils-merge');
var path = require('path');

/*
 * Installs support for rendering views
 *
 * Attaches a "renderView" method to req.la.   Default view engine is handlebars
 * Views are expected to be found in a views subfolder of the content root.
 */
module.exports = function initViewEngine(location, options)
{
	return function installViewEngine(la)
	{
		options = merge({
			"location": path.join(location, "views"),
			"defaultEngine": "hbs",
		}, options);


		la.renderView = require('../Utils/renderView')(options);
	}
}