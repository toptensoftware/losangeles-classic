var parseYamlPage = require('../Utils/parseYamlPage');
var deepcopy = require('deepcopy');
var merge = require('utils-merge');

/*
 * Installs support for loading yamlPages.
 * 
 * Files are expected to have .page extension.
 * Default content type is set to text/markdown - unless overridden in yaml header.
 */

module.exports = function installYamlPages(la)
{
	// Register file parser
	la.fileParsers[".page"] = function yamlPageParser(filename, callback_err_page)
	{
		// Load it
		parseYamlPage(filename, function parseYamlPageCallback(err, page) {

			// Throw error
			if (err)
			{
				callback_err_page(err);
				return;
			}

			// Done
			callback_err_page(null, page);
		});

	}
}