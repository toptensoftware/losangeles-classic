var async = require('async');
var merge = require('utils-merge');
var debug = require('debug')('losangeles:pageCore');

module.exports = function(la)
{
	// Setup filter stack
	la.filterStack = [];

	// Register a filter
	la.useFilter = function useFilter(filter)
	{
		this.filterStack.push(filter);
	}

	// Build the page
	la.buildPage = function buildPage(ctx, next)
	{
		// Fill in missing mandatory stuff
		if (!ctx.method)
			ctx.method = "get";
		if (!ctx.originalUrl)
			ctx.originalUrl = ctx.url;
		if (!ctx.query)
			ctx.query = {};

		// Setup ref back to self
		ctx.la = this;

		// Build the page
		debug("building page", ctx);
		async.applyEachSeries(this.filterStack, ctx,  function(err) {
			debug("page built", ctx);
			next(err);
		});
	}
}